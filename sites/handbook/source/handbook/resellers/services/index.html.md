---
layout: handbook-page-toc
title: "Channel Services Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
# GitLab Channel Services Program

The GitLab Channel Services program is designed to help new or existing partners design service portfolios around the DevOps Lifecycle in correlation to GitLab products. The program will help partners evaluate business opportunities and create a technical enablement framework to use as they build their GitLab-related service practices. 

The foundation of the GitLab Services Program consists of two elements on which partners can build upon to start or grow their GitLab-related service portfolio:



1. <a href="https://about.gitlab.com/handbook/resellers/services/#gitlab-certified-service-partner-program-overview">Certifications</a>:  GitLab offers GitLab Certified Service Partner Badges, that include training for service delivery teams which enable partners to meet program compliance requirements. 
2. <a href="https://partners.gitlab.com/prm/English/c/Services">GitLab Partner Portal Services Page</a>:  Partners can utilize GitLab Channel Service Packages to assist their teams as they build the service offerings, market and sell their services, and support their service business growth. 
    <br>a. <a href="https://partners.gitlab.com/prm/English/c/Channel_Service_Packages">GitLab Channel Service Packages</a>
    <br>b. <a href="https://partners.gitlab.com/prm/English/c/GitLab_Channel_Service_Sales_GTM">GitLab Channel Service Sales Got To Market</a>
    <br>c. <a href="https://partners.gitlab.com/prm/English/c/GitLab_Channel_Service_Development_Framework">GitLab Channel Service Development Framework</a>

## GitLab Certified Service Partner Program Overview

The GitLab Certified Service Partner Program offers our Partners three Certifications:


<table>
  <tr>
   <td style = "text-align: center">Available November 2020
   </td>
   <td style = "text-align: center">Coming in Q4 FY23
   </td>
   <td style = "text-align: center">Available by Invite March 2022
   </td>
  </tr>
  <tr>
   <td style = "text-align: center">




<img src="/images/channel-service-program/gitlab-professional-services-partner.png" width="150" alt="" title="GitLab Certified Professional Services Partner">

   </td>
   <td style = "text-align: center">




<img src="/images/channel-service-program/gitlab-managed-services-partner.png" width="150" alt="" title="GitLab Certified Managed Services Partner">

   </td>
   <td style = "text-align: center">




<img src="/images/channel-service-program/gitlab-training-services-partner.png" width="150" alt="" title="GitLab Certified Training Partner">

   </td>
  </tr>
  <tr>
   <td style = "text-align: center">GitLab Certified Professional Services Partner (PSP)
<br> 
<br>Distinguished partners with validated service capabilities  and a record for delivering excellent customer value through professional services.
   </td>
   <td style = "text-align: center">GitLab Certified Managed Services Partner (MSP)
<br> 
<br>Distinguished partners with validated service capabilities  and a record for delivering excellent customer value through managed services.
   </td>
   <td style = "text-align: center">GitLab Certified Training Partner (CTP)
<br> 
<br>
<br>Distinguished partners with validated learning and development capabilities and a record for delivering excellent customer experience through training services.
   </td>
  </tr>
</table>



<br>We have designed the GitLab Certified Service Partner Program to help partners who focus on offering services as a key foundation in their business model. These services can include:  



*  Assessment
*  Migration
*  Integration
*  Implementation
*  Education
*  Optimization
*  Managed/hosted services
*  Security/compliance

Becoming an authorized GitLab partner is the first step to becoming a Certified Service Partner and allows you to engage with the GitLab sales team and resell or refer GitLab products to your customers. Partners can achieve the GitLab Service Partner Program badges in addition to their program status to differentiate through their service offerings and unlock both financial and non-financial incentives as they achieve Certified Service Partner Badges.

Whether you are a new partner just getting started with a service portfolio or you already have a thriving service business that you are looking to grow through offering services around GitLab, our Certified Service Partner Program will offer you opportunities to build a successful business with GitLab.

## GitLab Certified Service Partner Requirements

The GitLab Certified Service Partner framework is designed to give partners multiple paths to success by focusing on the service priorities that are aligned to their individual business model. 

Partners can achieve multiple certifications or focus on one category based on their chosen areas of focus and investment choices. 



<div align= "center"><img src="/images/channel-service-program/q3fy21-gitLab-certified-service-partner-program.png" width="" alt="GitLab Certified Service Partner Program" title="image_tooltip"></div>



## GitLab Certified Service Partner Requirements and Progression

At GitLab, Certified Service Partner Certifications are meant to recognize service delivery excellence, technical expertise, and customer success among our partners for GitLab use cases in the DevOps lifecycle. Achieving a GitLab Service Partner Certification allows you to enhance your service capabilities and unlock key partner benefits in our program. 



<div align = "center"><img src="/images/channel-service-program/service-program-requirements-and-progression.png" width="" alt="alt_text" title="image_tooltip"></div>

<br>
## Becoming a Certified Service Partner

All GitLab Certified Service Partner tracks have program compliance and training requirements that must be completed before you can obtain the associated certification.

When the contractual requirements are met, and your organization sponsors the required number of practitioners who complete the training requirements and obtain the associated badges, your company will earn the related GitLab Service Partner Certification. 

The GitLab Channel team will communicate the award in email, and reflect the certification in the GitLab Partner Locator. 

All GitLab Certified Service Partner certifications are reviewed periodically.  Non- compliant partners at the time of the review will have one quarter to return to compliance.

### GitLab Certified Professional Services Partner Requirements
<div align= "center"><img src="/images/channel-service-program/gitlab-professional-services-partner.png" width="200" alt="GitLab Certified Service Partner Program" title="image_tooltip"></div>

<table>
   <tr>
      <td>

  <h3>Program Entry Requirements</h3>

      </td>
      <td>Each PSP must be:
      
      <br>* Be a Open or Select GitLab Partner


       <br>* Design, build and operate a professional service practice, and 
       <br>* Hire team members who have completed the competency requirements and/or sponsor the appropriate number of team members through completion of the competency requirements
 

       
       </td>
   </tr>
   <tr>
      <td><h3>Competency Requirements</h3>
      </td>
      <td>
      Each PSP must perpetually employ:



      <br>*   at least two (2) <a href="https://about.gitlab.com/handbook/resellers/training/">GitLab Verified Sales Core</a> 
      <br>*   at least one (1) <a href="https://about.gitlab.com/handbook/resellers/training/">GitLab Verified Solution Architect,</a> and 
      <br>*   at least three (3) <a href="https://about.gitlab.com/services/pse-certifications/pse-specialist/">GitLab Certified Professional Service Engineers</a> 
      <ol>
         <li>Partner organizations who achieved their PSP prior to Nov. 4, 2021 are required to have three (3) <a href="https://about.gitlab.com/services/pse-certifications/pse-specialist/">GitLab Certified Professional Service Engineers</a> on staff prior to August 2, 2022</li>
      </ol>
      </td>
  </tr>
  <tr>
     <td><h3>Service Offerings</h3>
     </td>
     <td>Any or all of the following services can be provided by an PSP: 
     <br>* Assessment
     <br>* Migration
     <br>* Integration
     <br>* Implementation
     <br>* Optimization
     <br>* Security/Compliance
     </td>
  </tr>
  <tr>
     <td><h3>Compliance Requirements</h3>
     </td>
     <td>Each PSP must:
     <br>* Hire and continually employ team members who achieve and maintain the competency requirements 
     <br>* Maintain positive Customer Success ratings measured against the following <a href="https://about.gitlab.com/handbook/customer-success/vision/#time-to-value-kpis">Time-to-Value KPIs</a> that GitLab uses for its metrics: Infrastructure Ready, First Value, & Outcome Achieved.
     </td>
     
  </tr>
</table>

<br>
### GitLab Certified Training Partner Requirements  

<div align= "center"><img src="/images/channel-service-program/gitlab-training-services-partner.png" width="200" alt="GitLab Certified Service Partner Program" title="image_tooltip"></div>

<table>
  <tr>
     <td>

     <h3>Program Entry Requirements</h3>

     </td>
     <td>* Each CTP must:


     <br>* Be a GitLab Open or Select GitLab Partners


     <br>* Design, build and operate a training services practice
     <br>* Complete the CTP contract Master Service Agreement and legal exhibit
     <br>* Hire team members who have completed the competency requirements and/or sponsor the appropriate number of team members through completion of the competency requirements
     <br>* Get setup on Edcast and participate in onboarding to access the course catalog


     </td>
  </tr>
  <tr>
     <td>
     <h3>Competency Requirements</h3>
     </td>
     <td>
      
     Each CTP must perpetually employ:




     <br>*   at least two (2) <a href="https://about.gitlab.com/handbook/resellers/training/">GitLab Verified Sales Professional</a> 
     <br>*   at least one (1) <a href="https://about.gitlab.com/handbook/resellers/training/">GitLab Verified Solution Architect</a>, and  
     <br>*   at least two (2) <a href="https://about.gitlab.com/handbook/customer-success/professional-services-engineering/gitlab-certified-trainer-process/">GitLab Certified Trainers</a> 
     <ol>
     <li>     Each CTP organization must be able to deliver at least four (4) GitLab courses</li>
     <li>     Each CTP organization must complete the competency requirements prior to 6 months after the last day of the quarter in which they enrolled in the certification process.</li> 
     </ol>
     </td>
  </tr>
  <tr>
      <td>
      <h3>Service Offerings</h3>
      </td>
      <td>
      CTP can provide <a href="https://about.gitlab.com/services/education/">GitLab Education Services</a> by purchasing GitLab student kits and then delivering instructor-led training courses in GitLab Learn.
      </td>
  </tr>
  <tr>
     <td>
     <h3>Compliance Requirements</h3>
     </td>
     <td>
     Each CTP must:
     <br>* Hire and continually employ team members who achieve and maintain the competency requirements
     <br>* Ensure that GitLab licensed courses are only be delivered by a GitLab Certified Trainer who is certified for that specific course   
     <br>* Maintain Customer Satisfaction (CSAT) score for training deliveries of at least 80%
     <ol>
     <li>  CSAT = (Number of satisfied customers - 4s and 5s) ÷ (Number of survey responses) x 100</li>
     <li>  Customer Satisfaction Surveys must be supplied prior to 30 days after the course delivery completion date</li>
     </ol>
     <br>* Submit service attach registration for each GitLab licensed training engagement within 30 days of the delivery completion date
     </td>
  </tr>

</table>


## GitLab Certified Service Partner Program Benefits

There is significant business opportunity for partners to offer and deliver GitLab enabled services, and in order to ensure there is consistent customer experience throughout the market we offer Service Partners certifications.

Each Certification track offers unique benefits that help partners better prepare themselves to deliver customer value through services directly to our joint customers. 

### Service Enablement Benefits

<table>
  <tr>
   <td colspan="4" >


   GitLab recognizes the important role our partners play in delivering value and services to our joint customers. To ensure partners have the latest sales and technical knowledge about our products, we offer many different training opportunities and technical resources. Partners can learn in a self-paced environment, on-line as well as various in-person, instructor-led classes. Training Benefits give partners the opportunity to pursue accreditations and certifications.
   </td>
  </tr>
  <tr>
   <td rowspan="6" >Training Benefits and Technical Resources
   </td>
   <td>Open/Select
   </td>
   <td>PSP
   </td>
   <td>CTP
   </td>
  </tr>
  <tr>
   <td colspan="3" >Quarterly updates and training events help our partners achieve and maintain accreditations and certifications. 
   </td>
  </tr>
  <tr>
   <td colspan="3" >Training discounts for GitLab instructor-led courses. 
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="2" >Service Partner Certifications: Recognizes customer success performance for your organization
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="2" >Certifications: Recognize service capabilities among your delivery teams
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="2" >Access to the <a href="https://about.gitlab.com/handbook/resellers/services/#partner-service-community">Partner Service Community</a>
   </td>
  </tr>
</table>
  

### Marketing Benefits

<table>
  <tr>
   <td colspan="4" >


   GitLab Open and Select partners have access to the proposal-based <a href="https://about.gitlab.com/handbook/resellers/#the-marketing-development-funds-mdf-program">GitLab Marketing Development Funds (MDF) Program</a>, which provides funding support for eligible marketing and partner enablement activities. Select partners who are also GitLab Certified Service Partners will have the highest visibility in the market. 
   </td>
  </tr>
  <tr>
   <td rowspan="5" >Services Related Marketing Benefits
   </td>
   <td>Open/Select
   </td>
   <td>PSP
   </td>
   <td>CTP
   </td>
  </tr>
  <tr>
   <td colspan="3" >MDF is available to help partners promote service offerings.
   </td>
  </tr>
  <tr>
   <td colspan="3" >Partner Locator: find partners with capabilities to address specific outcomes.
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="2" >Badging (specific to the GitLab Certified Service Partner  track)
   </td>
  </tr>
</table>
  

### Sales Acceleration

<table>
  <tr>
   <td colspan="4" >


   All authorized GitLab partners are eligible for co-selling with the GitLab Sales team. Certified partners are often prioritized in co-selling activities because those partners are able to deliver greater value for our joint customers. The GitLab Sales team and partner sellers align to drive customer value through joint account planning, account development, and working together to grow revenue.  Additionally, Certified Service Partners can be brought in for services only opportunities to help customers assess their DevOps infrastructure in advance of a potential GitLab purchase, or optimize their GitLab deployments.
   </td>
  </tr>
  <tr>
   <td rowspan="9" >Sales Acceleration
   </td>
   <td>Open/Select
   </td>
   <td>PSP
   </td>
   <td>CTP
   </td>
  </tr>
  <tr>
   <td colspan="3" >Introduction to GitLab sales team for account planning
   </td>
  </tr>
  <tr>
   <td colspan="3" >Introduction to sales opportunities by the GitLab Sales team
   </td>
  </tr>
  <tr>
   <td colspan="3" >Internal Use Licenses: Offering discounted licenses for internal use
   </td>
  </tr>
  <tr>
   <td colspan="3" >No cost Not for Resale (NFR) licenses: for lab testing, demos, training and educational use incremental NFRs given to PSP and MSP
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="2" >Service Packages : GitLab Services toolkits, sales and marketing IP
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="2" >Opportunities for co-delivering services
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>
   Program designed to support Partner Training Service Business Model
   </td>
  </tr>
  <tr>
   <td colspan="2" >
   </td>
   <td>
   Eligible to be nomiated to provide subcontracting services to GitLab Education Services
   </td>
  </tr>
</table>



<br>
## Partner Service Community


### Overview

This community is designed to allow partner service delivery teams to directly collaborate on project specific challenges with the internal GitLab technical teams. The community will benefit the GitLab Certified Service Partners by:



1. Extending the technical service enablement beyond the certification curriculum and into the real world. 
2. Serving as a knowledge base of delivery patterns and best practices.
3. Reducing delivery risk by applying best practices of GitLab practitioners worldwide.  

Learn about this community in [this video](https://www.brighttalk.com/webcast/18613/472525). 




### Accessing the Community

When a partner is awarded a GitLab Certified Service Partner certification, the partner account administrator will be invited to join this [community](https://gitlab.com/gitlab-com/channel/partners/services-helpdesk). The partner account administrator can reference [this video](https://www.brighttalk.com/webcast/18613/472272) to learn how to add and manage users who can access this project.

You can access this community [here](https://gitlab.com/gitlab-com/channel/partners/services-helpdesk). 


### User Commitments to the Community

As a company, GitLab is dedicated to open source, and this community is one of the ways we are implementing that while supporting partners to create excellent customer experiences. Access to this community will only be beneficial if the users commit to being part of a community. Below are a few rules to live by when using this community. 



1. Give as much as you take 
2. Keep a tidy board 
3. Do not share customer identifiable information when posting to an issue in this project.

You can find out how to be a great community member in [this video](https://www.brighttalk.com/webcast/18613/472541).



## Maintaining Service Partner Certifications

At GitLab, collaboration and feedback is meant to nurture and mentor our certified service partners to grow their service capabilities and expertise. The program will utilize our internal [customer success metrics](https://about.gitlab.com/handbook/customer-success/vision/#measurement-and-kpis) to measure and understand the GitLab customer experience across the entire ecosystem and help further develop our certified service partners. In order to work together in this process, we will ask each certified service partner to provide evidence (as stated in the program descriptions above) to support the periodic  review  for your customer set one month before it is due. 

GitLab Channel Partner program will review the GitLab Certified Service Partners’ customer success rating and practitioner certification status each year in the month the partner was originally granted certification. Partners will be notified with the outcome of the review and the resulting status of their certification. If the partner needs to make further investments to stay in good standing with regards to Service Partner Certifications, the email will indicate the existing status and state the expected status with a deadline when compliance is required to renew your GitLab Service Partner Certification. Partners can work with their GitLab Channel Account Manager (CAM) to create a plan for obtaining and maintaining certifications.
