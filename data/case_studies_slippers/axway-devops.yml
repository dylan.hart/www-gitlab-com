title: Axway Software
file_name: axway-software
cover_image: /images/blogimages/axway_casestudy_2.png
cover_title: |
  Axway aims for elite DevOps status with GitLab
cover_description: |
  After years of working with GitLab for SCM, Axway’s digital transformation reaches new heights.
canonical_path: /customers/axway-devops/
twitter_image: /images/blogimages/axway_casestudy_2.png
twitter_text: After years of working with GitLab for SCM, Axway’s digital transformation reaches new heights.
customer_logo: /images/case_study_logos/axway-logo.svg
customer_logo_css_class: brand-logo-tall
customer_industry: Technology 
customer_location: 17 countries
customer_solution: GitLab Premium
customer_employees: 2,000 employees
customer_overview: >-
   Axway has been a GitLab user since 2016, and recently expanded its use to achieve CI/CD.
customer_challenge: >-
  Axway was looking for a CD tool with security, fast feedback, and rapid deployment capabilities.

key_benefits: >-

    Faster feedback with developer collaboration in a single tool

    Cost savings

    Supports AWS

    Improved collaboration

    Security starts sooner

    Utilizing blue/green deployments

    Achieving daily deploys
customer_stats:
  - stat: <15%
    label: Change failure rate
  - stat: 12x
    label: Improvement in service deliveries
  - stat: <2
    label: Hours to resolve any outage
customer_study_content:
  - title: the customer
    subtitle: Leading API gateway vendor
    content: >-

        Axway Software is the last independent API gateway vendor on the market, providing on-premise and cloud software to customers. Axway’s hybrid integration platform, AMPLIFY™ combines traditional integration patterns with APIs and application integration using over 150 pre-built connectors. 11,000 organizations worldwide rely on Axway for their integration needs.

        
        “When you connect to your bank via your smartphone to retrieve your account balance or complete a transfer, you expect that connection to be secure. [Axway](https://www.axway.com/en) provides the software behind the scenes to securely complete your transaction. We provide the infrastructure or the software behind it to do the secure transfer of your money and so on. We provide critical services to our customers’ business,” said Eric Labourdette, VP cloud operations, Axway.

        
        Axway fully transitioned from Subversion (SVN) to [GitLab in 2016](/customers/axway/) as a source code management solution. At the same time, they transitioned from VersionOne to JIRA/Confluence as an issue and backlog management solution. The migration was a change not only from a tooling standpoint, but also from the product standpoint. “We started with a product and now we are moving to a platform on the cloud and the cloud represents 50% of the revenue we get compared to license revenue,” Labourdette said. The company previously had 10 source control system servers around the world and now the company has one centralized server with GitLab.

  - title: the challenge
    subtitle: Requiring elite performance, security, and speed
    content: >-

        Like most companies that provide quality service to their customers, Axway is focusing on cloud delivery. The company was looking to enhance software delivery and operational performance. Specifically, their objectives were around deployment frequency, change failure rate, and time to restore, without losing the core performance strengths of stability and security. Developers were also searching for more visibility and improved collaboration. They needed a tool that provided developers with faster feedback and had all the data in one place.

        
        Axway serves nine out of 10 of the world’s largest banks, which means that security continues to be a priority. Using the most recent Accelerate State of DevOps Report as inspiration, the team at Axway has set a goal to be an elite performer by the report’s standards by 2020. In the 2018 report, only 7% percent of 2,500 companies surveyed were considered elite performers. The 2019 report showed 20% of respondents achieved the status.

        
        According to the DevOps Research and Assessment (DORA) Group, elite companies:

        * Deploy code to production multiple times a day

        * Commit code to successfully run in production in less than a day

        * Restore service after an incident is less than an hour

        * Have a change failure rate of less than 15%

        To accomplish this goal they applied the immutability concept and applied it to their toolchain in 2018. Their teams had the same copy of Jenkins deployed as a Docker image and pulled their pipelines from GitLab and ran that way. But that methodology only worked while the SaaS product was in its infancy with approximately 15 microservices. Growth and maturation led to more than 24 microservices, and the team was unable to maintain the toolchain.

  - title: the solution
    subtitle: Developers prefer Gitlab
    content: >-

        As Axway has ramped up its continuous deployment, GitLab has become an intricate tool in the CI/CD process. “The good thing is GitLab has been with us for a long time and moves along with our evolution,” Labourdette said. “GitLab developed a lot of features that we've seen before and provides the full value stream. We transitioned to Gitlab to have a single point of contact system, so we don't have to jump from multiple tools and that helps developers to deliver faster.”

        
        In 2019, Axway was able to achieve its desired deployment frequency with improvements to the microservice management. Every microservice has the same templated pathway to get to production. “If you want to build new microservices on our team, you get a new repo, you get all the tools, you get the same pathway,” said Vince Stammegna, Senior Director of Engineering, SaaS. “We call them paved roads and developers can go and build their service and have access to the tools they need to be self-service. We're not 100% there yet, but we're getting there.”

        
        Teams are now consistently seeing:

        * SaaS code between once an hour to once per day

        * Lead time for change is between one day and one week

        * Time to restore service is less than a day

        * Change failure rate is less than 15%

        By the end of 2020, the team plans to fully transition to [GitLab CI](/stages-devops-lifecycle/continuous-integration/) and move away from Jenkins entirely, primarily because of developer feedback. The prominent pain point with having a multi-toolchain was the inability to find all the data in one place. GitLab provides the company with natively built integrated tools that are built into the CI pipeline. “Moving all of our pipelines into GitLab gives our developers faster feedback every day and it's one less tool to manage. It's much easier when you're reasoning about the service you're building to have everything in the repo for the service, rather than have to fish around on Jenkins and go hunting to figure out where things broke down,” Stammegna said. “So all of our security scans such as Fortify and Twistlock are actually done within the CI pipeline and GitLab so we get results back.”
  - title: the results
    subtitle: Secure pipelines and faster SaaS deployments
    content: >-

        Moving all of their pipelines to GitLab has given developers faster feedback, a clean dashboard, and one less tool to manage. It also allows the team to move toward continuous deployment, with security at the forefront.

        
        An initial security review and final security review would take place at the beginning and end of a quarterly release. This caused delays in the product release because developers did not have enough time to remediate security vulnerabilities before the release date. “Now it's moved to what's called continuous security review, taking all these tools and giving the developers feedback instantaneously after every build and deployment so they know whether or not they're going to pass the security bar,” Stammegna said.

        
        GitLab CI helps to seamlessly and securely manage deployments across many AWS accounts. Axway has been deploying their cloud offerings on AWS for over six years and is an AWS Advanced Technology Partner. “AWS is the leader in the Infrastructure as a Service market. The speed of delivery of services and innovation is unmatched. AWS has a high level of security compliance and services and provides a partnership to support us into the cloud journey,” Labourdette said. Axway uses a wide range of AWS services such as EC2, S3, EBS and PrivateLinks, as well as RDS managed services based on business needs. The AMPLIFY™ platform is container based and uses Kubernetes and EKS for the orchestration, following continuous deployment patterns.

       
        With all the necessary steps in place within the pipeline, deployment frequency has sped up. SaaS is deployed once per hour and once per day, using the updated build flow. "Lead time to change is between one and seven days, depending on the service. Our mean time to restore service is less than one day and we've never had an outage last longer than two hours. Finally, our change failure rate is less than 15% to date," Stammegna said.
customer_study_quotes:  
  - blockquote: If you want to speed up the delivery cycle, you need to simplify your ecosystem. And we've been doing that with GitLab along the way, it's critical for developers to have one single point of contact and one simple interface to increase the speed of delivery.
    attribution: Eric Labourdette
    attribution_title: VP cloud operations, Axway
  - blockquote: We need to foster collaboration. It's why we are all on GitLab. We are all on the same ecosystem with teams who are working in pretty much every time zone.
    attribution: Eric Labourdette
    attribution_title: VP cloud operations, Axway
